#include "utils.h"

void error(const char* _err, int _errno)
{
    log("[ERROR] (0x%X) %s\n", _errno, _err);
}

void log(const char* _text, ...)
{
    struct tm *timeStruct;
    time_t timer;
    time(&timer);
    timeStruct = localtime(&timer);

    char *tmp = new char[12+strlen(_text)];
    strftime(tmp, 13, "[%H:%M:%S] ", timeStruct);
    strcat(tmp, _text);

    va_list args;
    va_start(args, _text);
    vfprintf(stdout, tmp, args);

    char *name = new char[40];
    strftime(name, 40, "./logs/logCore_%d-%m-%Y_%H-%M-%S.log", timeStruct);

    FILE *logFile;

    #if defined(_WIN32)
    CreateDirectory("./logs", NULL);
    #endif
    #if defined(__linux__) || defined(__unix__)
    mkdir("./logs", S_IRWXU);
    #endif // defined

    static unsigned int index = LOG_FILE_NOT_CREATE;

    if(index == LOG_FILE_NOT_CREATE)
    {
        logFile = fopen(name, "w");
        if (logFile == NULL)
        {
            exit(-1);
        }
        index = LOG_FILE_CREATE;
    }
    else
    {
        logFile = fopen(name, "a");
    }
        vfprintf(logFile, tmp, args);



    va_end(args);

    fclose(logFile);
    delete[] tmp;
}
