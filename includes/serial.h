#ifndef _SERIAL_H_
#define _SERIAL_H_

#include <stdio.h>
#include "../utils.h"

#if defined(_WIN32)
#include <windows.h>
#endif // _WIN32

#if defined(__linux__) || defined(__unix__)
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#endif // defined

#define DEFAULT_SIZE_COMNAME 20*sizeof(char)
#define DEFAULT_COMNAME "\\\\.\\COM1"

#define SERIAL_BUFFER_SIZE 100

class Serial
{
#if defined(_WIN32)
    HANDLE hSerial;
    DCB dcbCOMParam = {0};

    HANDLE hThreadRead;
    HANDLE hMutexRead;
    DWORD WINAPI ThreadReadProc(LPVOID _param);

    LPCSTR lpCOM;
#endif // _WIN32
#if defined(__linux__) || defined(__unix__)
    int fSerial;
    char *nameCOM;
#endif // defined
    int addToBuffer(const char* _data);
    char* readFromBuffer(unsigned int _num);
    unsigned int countBufferData = 0;
    char **inBuffer;
public:
    Serial();
    ~Serial();
    void sendText(const char*);
    int init(const char* _nameCOM);
};

#endif // _SERIAL_H_
