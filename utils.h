#ifndef _UTILS_H_
#define _UTILS_H_

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdarg.h>
#include <time.h>

#if defined(_WIN32)
#include <windows.h>
#endif // defined
#if defined(__linux__) || defined(__unix__)
#include <sys/types.h>
#include <sys/stat.h>
#endif // defined

#define LOG_FILE_CREATE 0
#define LOG_FILE_NOT_CREATE 1

void error(const char* _err, int _errno);
void log(const char* _text, ...);

#endif // _UTILS_H_
