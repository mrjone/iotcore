#include "../includes/serial.h"

#if defined(_WIN32)
DWORD WINAPI Serial::ThreadReadProc(LPVOID _param)
{
    DWORD waitMsg,
        readByte;
    while(1)
    {
        waitMsg = WaitForSingleObject(this->hMutexRead, INFINITE);
        switch(waitMsg)
        {
        case WAIT_OBJECT_0:
            {
                break;
            }
        }
    }

    ExitThread(0);
}
#endif // defined

Serial::Serial()
{
    this->inBuffer = NULL;
    #if defined(_WIN32)
    this->lpCOM = (LPCSTR)malloc(DEFAULT_SIZE_COMNAME);
    #endif // defin���ed

    #if defined(__linux__) || defined(__unix__)
    this->nameCOM = (char*)malloc(DEFAULT_SIZE_COMNAME);
    #endif // defined
    log("Start SERIAL...\n");
}

Serial::~Serial()
{
    log("End SERIAL...\n");

    delete[] this->inBuffer;

    #if defined(_WIN32)
    CloseHandle(this->hSerial);
    CloseHandle(this->hThreadRead);
    CloseHandle(this->hMutexRead);
    delete[] this->lpCOM;
    #endif // define

    #if defined(__linux__) || defined(__unix__)
    close(this->fSerial);
    delete[] this->nameCOM;
    #endif // defined
}

void Serial::sendText(const char* _text)
{

}

int Serial::init(const char* _nameCOM)
{
    #if defined(_WIN32)
    strcpy((char*)this->lpCOM, _nameCOM);
    this->hSerial = CreateFile(this->lpCOM,
                               GENERIC_READ | GENERIC_WRITE,
                               0,
                               NULL,
                               OPEN_EXISTING,
                               FILE_FLAG_OVERLAPPED,
                               NULL);
    if (this->hSerial == INVALID_HANDLE_VALUE)
    {
        DWORD lastErr = GetLastError();
        switch (lastErr)
        {
        case ERROR_FILE_NOT_FOUND:
            error("COM port not found. Edit port or check connection!", lastErr);
            break;
            //TODO: ��������� ��� ������ ����. �� ������ ������� ���!
        default:
            error("undefined error code = INVALID_HANDLE_VALUE, check config COM port", lastErr);
            break;
        }
        return 1;
    }
    #endif // defined

    #if defined(__linux__) || defined(__unix__)
    strcpy(this->nameCOM, _nameCOM);
    this->fSerial = open(this->nameCOM,
            O_RDWR
            | O_NONBLOCK
            | O_NOCTTY
            | O_NDELAY);
    if (this->fSerial == -1)
    //Cannot open Serial
    {
        switch(errno)
        {
        case EEXIST:
            error("COM port alrady used!", errno);
            break;
        case ENODEV:
            error("COM port not found. Edit port or check connection!", errno);
            break;
        case EACCES:
            error("Cannot get acces of COM", errno);
            break;
        default:
            error("undefined error code OPEN(), check config COM port", errno);
            break;
        }
        return 1;
    }
    #endif // defined

    log("SERIAL setup name: %s\n", _nameCOM);

    #if defined(_WIN32)
    this->dcbCOMParam.DCBlength = sizeof(this->dcbCOMParam);
    if (!GetCommState(this->hSerial, &this->dcbCOMParam))
    {
        error("Cannot get COM state", GetLastError());
        return 1;
    }

    this->dcbCOMParam.BaudRate = CBR_9600;
    this->dcbCOMParam.ByteSize = 8;
    this->dcbCOMParam.StopBits = ONESTOPBIT;
    this->dcbCOMParam.Parity = NOPARITY;

    if (!SetCommState(this->hSerial, &this->dcbCOMParam))
    {
        error("Cannot set COM state", GetLastError());
        return 1;
    }
    log("SERIAL setup BR: %d; BS: %d;\n",
        this->dcbCOMParam.BaudRate,
        this->dcbCOMParam.ByteSize);

    this->hMutexRead = CreateMutex(NULL,
                                   FALSE,
                                   NULL);
    if (this->hMutexRead == NULL)
    {
        error("Cannot create SERIAL->MUTEX", GetLastError());
        return 1;
    }
    this->hThreadRead = CreateThread(NULL,
                                     0,
                                     (LPTHREAD_START_ROUTINE)&ThreadReadProc,
                                     NULL,
                                     0,
                                     NULL);
    if (this->hThreadRead == NULL)
    {
        error("Cannot create SERIAL->THREAD", GetLastError());
        return 1;
    }
    #endif // defined

    return 0;
}

int Serial::addToBuffer(const char* _data)
{
    char **tmpBuffer;
    if (this->inBuffer = NULL && this->countBufferData == 0)
    {
        this->countBufferData++;
        this->inBuffer = new char[this->countBufferData][SERIAL_BUFFER_SIZE];
    }
    else
    {
        tmpBuffer = new char[this->countBufferData][SERIAL_BUFFER_SIZE];
        for (int i = 0; i < this->countBufferData; i++)
            for (int j = 0; j < SERIAL_BUFFER_SIZE; j++)
                tmpBuffer[i][j] = this->inBuffer[i][j];
        //copy main buffer in tmp buffer

        for (int i = 0; i < SERIAL_BUFFER_SIZE; i++)
            delete[] this->inBuffer[i];
        delete[] this->inBuffer;
        //clear old buffer

        this->countBufferData++;
        this->inBuffer = new char[this->countBufferData][SERIAL_BUFFER_SIZE];

        for (int i = 0; i < this->countBufferData-1; i++)
            for (int j = 0; j < SERIAL_BUFFER_SIZE; j++)
                this->inBuffer[i][j] = tmpBuffer[i][j];
        //copy tmp buffer into main buffer
    }
}

char* readFromBuffer(unsigned int _num)
{

}
